# Create a function that, given an array of integers, 
# finds out the difference between any two numbers 
# such that the larger number appears after the smaller number in the array.

# Input : arr = [2, 3, 8, 6, 4, 8, 1]
# Output : 6
# Explanation : The maximum difference is between 8 and 2.

# Input : arr = [ 7, 9, 5, 6, 13, 2 ]
# Output : 8
# Explanation : The maximum difference is between 13 and 5.

def find_max_diff(array: [])
    @max_diff = 0

    while array.length > 0
        this = array.shift

        array.each do |num|
            diff = num - this
            @max_diff = diff if diff > @max_diff
        end
    end

    @max_diff
end

array_1 = [2, 3, 8, 6, 4, 8, 1]     # => 8
array_2 = [7, 9, 5, 6, 13, 2]       # => 6
array_3 = [5, 7, 1, 3, 5, 2, 5, 1]  # => 4

print "Array 1 returns "
p find_max_diff(array: array_1)

print "Array 2 returns "
p find_max_diff(array: array_2)

print "Array 3 returns "
p find_max_diff(array: array_3)


# =========The "Ben Flowers" Method=============

input = [2, 3, 8, 6, 4, 8, 1]

input2 = [7, 9, 5, 6, 13, 2]

input3 = [5, 7, 1, 3, 5, 2, 5, 1]

def build_results(input: [])
  results = []
  # For the length of the array
  input.length.times do |item|
    # Select the first number
    selected_item = input[item]
    next_element = item + 1
    # For ever element ahead of that item until the end
    input[next_element..input.length].each do |x|
      # If selected is less than what is ahead
      if selected_item < x
        # Add the difference to results
        results << x - selected_item
        p results
      end
    end
  end
  return results.max
end

p '=========This is the Ben Flowers method========='
p build_results(input: input)
p build_results(input: input2)
p build_results(input: input3)
